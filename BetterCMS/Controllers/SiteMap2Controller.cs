﻿using BetterCms.Module.Api;
using BetterCms.Module.Api.Operations.Pages.Sitemap;
using BetterCms.Module.Api.Operations.Pages.Sitemap.Tree;
using BetterCMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BetterCMS.Controllers
{
    public class SiteMap2Controller : Controller
    {
        private static Guid defaultSitemapId = new Guid("17ABFEE9-5AE6-470C-92E1-C2905036574B");

        //
        // GET: /SiteMap/
        public ActionResult Index(Guid actualPageId)
        {
            List<MenuItemViewModel> model = new List<MenuItemViewModel>();
            using (IApiFacade apiFacade = ApiFactory.Create())
            {
                Guid? sitemapId = this.GetSitemapId(apiFacade);
                if (sitemapId.HasValue)
                {
                    GetSitemapTreeRequest request = new GetSitemapTreeRequest
                    {
                        SitemapId = sitemapId.Value
                    };
                    GetSitemapTreeResponse getSitemapTreeResponse = apiFacade.Pages.Sitemap.Tree.Get(request);
                    if (getSitemapTreeResponse.Data.Count > 0)
                    {
                        model = (
                            from mi in getSitemapTreeResponse.Data
                            select new MenuItemViewModel
                            {
                                Caption = mi.Title,
                                Url = mi.Url,
                                IsActive = mi.PageId == actualPageId
                            }).ToList<MenuItemViewModel>();
                    }
                }
            }
            return base.View(model);
        }

        private Guid? GetSitemapId(IApiFacade api)
        {
            GetSitemapsResponse getSitemapsResponse = api.Pages.Sitemap.Get(new GetSitemapsRequest());
            if (getSitemapsResponse.Data.Items.Count > 0)
            {
                SitemapModel sitemapModel = getSitemapsResponse.Data.Items.FirstOrDefault((SitemapModel map) => map.Id == SiteMap2Controller.defaultSitemapId) ?? getSitemapsResponse.Data.Items.First<SitemapModel>();
                return new Guid?(sitemapModel.Id);
            }
            return null;
        }
    }
}
